## Goal of the project
The intent of the project is to provide for hadith searchers a sophisticated way to look for hadiths and give a score based on what is
already done on Hadith books.

We aim to provide a well structured database of texts reported to Prophet Mohamed SAWS that enhance searching in content and people that


## Terminology
- **MATN**= content of the "REWAYA"
- **SANAD**= people involved in the path until Prophet Mohamed SAWS
- **RAWI**= person having at least one **REWAYA**
- **REWAYA**= **MATN** + **SANAD**

## Main use cases
- Look for a specific **MATN** and its similar in the the database and display all **SANAD**s
- display all **MATN**s from a specific **RAWI**
- ???

## OPEN questions:
- Which technology to use?
- First version supported version ?
- Contribution and administration model and rights
- **RAWI** score evaluation ?
- **SANAD** score evaluation ?
- Identitification of **RAWI** ?
- Supported UIs (browser, mobile(Android, iOS, ..), Desktop) ?
